-------------------------------------------------------------
-- 文件名：location.lua
-- 功能：解析TCP透传到OneNET平台的坐标数据
-- 作者：wensong
-- 时间：2020.03.03
-- 邮箱：470868560@qq.com
-------------------------------------------------------------


--------------------------------------------------------------
-- 按照字符分割字符串                                --
-- @param   str   字符串                            --
-- @param   delimiter   分割字符                    --
-- @return  返回字符串数组                           --
-- @example local arr = split("123#321","#")       --
--			print(arr[1])
--			print(arr[2])
-- 来源：https://www.runoob.com/note/28521
--------------------------------------------------------------
function split(str,delimiter)
    local dLen = string.len(delimiter)
    local newDeli = ''
    for i=1,dLen,1 do
        newDeli = newDeli .. "["..string.sub(delimiter,i,i).."]"
    end

    local locaStart,locaEnd = string.find(str,newDeli)
    local arr = {}
    local n = 1
    while locaStart ~= nil
    do
        if locaStart>0 then
            arr[n] = string.sub(str,1,locaStart-1)
            n = n + 1
        end

        str = string.sub(str,locaEnd+1,string.len(str))
        locaStart,locaEnd = string.find(str,newDeli)
    end
    if str ~= nil then
        arr[n] = str
    end
    return arr
end

------------------------------------------------------------------------------------------
-- 设置定时下发设备的数据（可选）                                                       --
------------------------------------------------------------------------------------------
function device_timer_init(dev)
	-- 添加用户自定义代码 --
	-- 例如： --
	dev:timeout(3)
	dev:add(10,"dev1","hello")    --每10秒下发一包数据，内容为hello
end

------------------------------------------------------------------------------------------
-- 解析设备上传的数据（可选）                                                       --
-- 设备按照 经度#纬度 的格式上传，比如 12.1#9.2
------------------------------------------------------------------------------------------
function device_data_analyze(dev)
	local ds_name = "location"
	local a = 0

	-- 添加用户自定义代码 --
	-- 例如： --
	local data_len = dev:size()    --获取上行数据长度
	local arr_location = split(dev:bytes(1,data_len),"#");
	local format_pattern = '[{"v":{"lon":%f,"lat":%f},"i":"%s","a":0}]'
	local json_str = string.format(format_pattern, arr_location[1],arr_location[2], ds_name)

	dev:response()
	dev:send("received")  --发送应答

	-- return $1,$2 --
	-- 例如： --
	return data_len,json_str    --保存该数据
end