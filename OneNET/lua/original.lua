-------------------------------------------------------------
-- 文件名：original.lua
-- 功能：解析TCP透传到OneNET平台的原始数据
-- 作者：wensong
-- 时间：2020.04.21
-- 邮箱：470868560@qq.com
-------------------------------------------------------------

------------------------------------------------------------------------------------------
-- 设置定时下发设备的数据（可选）                                                       --
------------------------------------------------------------------------------------------
function device_timer_init(dev)
	-- 添加用户自定义代码 --
	-- 例如： --
	dev:timeout(3)
	dev:add(10,"dev1","hello")    --每10秒下发一包数据，内容为hello
end


function device_data_analyze(dev)
	local ds_name = "original"

	-- 添加用户自定义代码 --
	-- 例如： --
	local data_len = dev:size()    --获取上行数据长度
	local data_content = dev:bytes(1,data_len);
	local format_pattern = '[{"v":"%s","i":"%s","a":0}]'
	local json_str = string.format(format_pattern, data_content, ds_name)

	dev:response()
	if string.format("%s", data_content) == "howareyou" then
		dev:send("iamfine")  --发送应答
	else
		dev:send("received")
	end
	-- return $1,$2 --
	-- 例如： --
	return data_len,json_str    --保存该数据
end